package us.theappacademy.todo;

public class ToDoPost {
    public String title;
    public String date;
    public String due;
    public String work;

    public ToDoPost(String title, String date,String due,String work){
        this.title = title;
        this.date = date;
        this.due = due;
        this.work = work;
    }
}