package us.theappacademy.todo;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ToDoActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
    }
}
